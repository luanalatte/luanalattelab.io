// import Alpine from "alpinejs";
// import { navigate } from "astro:transitions/client";

export default () => ({
  history: $persist([]),
  historyF: $persist([]),
  maximized: $persist(false),
  flag: $persist(false),
  maximize() {
    this.maximized = !this.maximized;

    if (this.maximized) {
      $el.classList.remove('p-8');
      // $refs.window.dataset.state = 'maximized';
      $refs.window.classList.remove('unmaximized');
    } else {
      $el.classList.add('p-8');
      // $refs.window.dataset.state = 'unmaximized';
      $refs.window.classList.add('unmaximized');
    }
  },
  navigateFull() {
    url = $refs.urlbar.value.replace(window.location.origin, "");
    navigate(url.startsWith('/') ? url : '/' + url);
  },
  back() {
    if (this.history.length > 0) {
      this.historyF.push(window.location.href);
      this.flag = true;
      navigate(this.history.pop() ?? "/");
    }
  },
  forwards() {
    if (this.historyF.length > 0) {
      this.history.push(window.location.href);
      this.flag = true;
      navigate(this.historyF.shift() ?? "/");
    }
  },
  reload() {
    this.url = "";
    this.history = [];
    this.historyF = [];
  },
  beforeSwap() {
    if (!this.flag) {
      this.history.push(window.location.href);
      this.historyF = [];
    }
    this.flag = false;
  }
});
