const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    container: {
      center: true,
    },
    fontFamily: {
      sans: ["Lexend Deca", ...defaultTheme.fontFamily.sans],
    },
    extend: {
      boxShadow: {
        "sharp-sm": "1px 1px 0 rgba(0, 0, 0, 0.25)",
        sharp: "2px 2px 0 rgba(0, 0, 0, 0.25)",
        "sharp-lg": "4px 4px 0 rgba(0, 0, 0, 0.25)",
        "sharp-xl": "6px 6px 0 rgba(0, 0, 0, 0.25)",
      },
    },
  },
  plugins: [],
};
